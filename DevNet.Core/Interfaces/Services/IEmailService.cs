﻿using DevNet.Core.Models.Emails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DevNet.Core.Interfaces.Services
{
    public interface IEmailService
    {
        Task SendForgotEmail(ForgotEmail model);
        Task SendEmailAsync(BaseEmail model);
        Task SendRegisterEmail(RegisterEmail model);
    }
}
