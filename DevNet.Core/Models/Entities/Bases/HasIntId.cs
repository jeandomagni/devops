﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models.Entities.Bases
{
    public class HasIntId
    {
        public HasIntId()
        {
            CreateDate = DateTime.UtcNow;
        }

        public int Id { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
