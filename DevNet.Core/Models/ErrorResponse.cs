﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models
{
    public class ErrorResponse<T>
    {
        public ErrorResponse()
        {

        }
        public ErrorResponse(T data)
        {
            Data = data;
        }

        public T Data { get; set; }
    }
}
