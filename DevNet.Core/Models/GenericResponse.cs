﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models
{
    public class GenericResponse<T>
    {
        public GenericResponse()
        {

        }
        public GenericResponse(T data)
        {
            Data = data;
        }

        public T Data { get; set; }
    }
}
