﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models.Emails
{
    public abstract class BaseEmail
    {
        public abstract string ViewName { get; }
        public string Title { get; set; }
        public string[] Emails { get; set; }
        public BaseEmail(string title, params string[] emails)
        {
            Title = title;
            Emails = emails;
        }
    }
}
