﻿using DevNet.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models.Emails
{
    public class RegisterEmail : BaseEmail
    {
        public RegisterEmail(string title, params string[] emails) : base(title, emails)
        {
        }

        public string ResetUrl { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string LoginUrl { get; set; }

        public override string ViewName => EmailViews.Register;
    }
}
