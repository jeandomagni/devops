﻿using DevNet.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models.Emails
{
    public class ForgotEmail : BaseEmail
    {
        public ForgotEmail(string title, params string[] emails) : base(title, emails)
        {
        }

        public string Email { get; set; }

        public string ResetUrl { get; set; }

        public override string ViewName => EmailViews.Forgot;
    }
}
