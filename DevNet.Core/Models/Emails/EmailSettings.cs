﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models.Emails
{
    public class EmailSettings
    {
        public string PrimaryDomain { get; set; }

        public int PrimaryPort { get; set; }

        public string UsernameEmail { get; set; }

        public string UsernamePassword { get; set; }
    }
}
