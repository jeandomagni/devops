﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Models.Account
{
    public class LoginResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
