﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevNet.Core.Constants
{
    public class EmailViews
    {
        public const string Forgot = "Forgot";
        public const string Register = "Register";
    }
}
