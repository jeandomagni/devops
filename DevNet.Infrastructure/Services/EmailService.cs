﻿using DevNet.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using DevNet.Core.Models.Emails;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;

namespace DevNet.Infrastructure.Services
{
    public class EmailService : IEmailService
    {
        private readonly IViewRenderService _viewRenderService;
        private readonly EmailSettings _emailSettings;
        public EmailService(IViewRenderService viewRenderService, IOptions<EmailSettings> emailSettings)
        {
            _viewRenderService = viewRenderService;
            _emailSettings = emailSettings.Value;
        }

        public async Task SendForgotEmail(ForgotEmail model)
        {
            await SendEmailAsync(model);
        }

        public async Task SendRegisterEmail(RegisterEmail model)
        {
            await SendEmailAsync(model);
        }

        public async Task SendEmailAsync(BaseEmail model)
        {
            var result = await _viewRenderService.RenderToStringAsync($"Emails/{model.ViewName}", model);

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write((String)result);
            writer.Flush();
            stream.Position = 0;

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("The Dev Net Team", "support@devnet.com"));

            foreach (var em in model.Emails)
            {
                message.To.Add(new MailboxAddress("Personal", em));
            }

            message.Subject = model.Title;
            var bodyBuilder = new BodyBuilder();

            bodyBuilder.HtmlBody = result;

            message.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.Connect(_emailSettings.PrimaryDomain, _emailSettings.PrimaryPort);
                //client.AuthenticationMechanisms.Remove("XOAUTH2");  // due to enabling less secure apps access
                Console.WriteLine("Prepairing the Email");
                try
                {
                    client.Authenticate(_emailSettings.UsernameEmail, _emailSettings.UsernamePassword);
                    Console.WriteLine("Auth Completed");
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR Auth");
                }
                try
                {
                    client.Send(message);
                    Console.WriteLine("Email had been sent");
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR");
                }
                client.Disconnect(true);
            }
        }
    }
}
