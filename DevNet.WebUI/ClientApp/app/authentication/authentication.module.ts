﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';           


import { AuthenticationRoutingModule } from './index';
import { SharedModule } from '../shared';

@NgModule({
    imports: [CommonModule, FormsModule, AuthenticationRoutingModule, SharedModule],
    declarations: [],
    providers: []  
})
export class AuthenticationModule { }