﻿import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';    
import { RouterModule, Routes } from '@angular/router';  

const routing: Routes = [
    
];

@NgModule({
    imports: [
        RouterModule.forChild(routing)
    ],
    exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
