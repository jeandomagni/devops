﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './home';

export const routes: Routes = [
    { path: '', component: IndexComponent },
    { path: 'authentication', loadChildren: 'authentication/authentication.module#AuthenticationModule' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
