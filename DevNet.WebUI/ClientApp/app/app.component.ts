import { Component, OnInit } from '@angular/core';
import { UserDetails, Message, CONSTANTS } from './shared';
import { AppContext, MessageService, LoaderService } from './core';
import { Subscription } from 'rxjs/Subscription';
 

import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,
    ActivatedRoute
} from '@angular/router'
declare var initMapIcons: () => void;
declare var appSettings: any;
import * as $ from 'jquery';

@Component({
    selector: 'app',
    template: require('./app.component.html')
})
export class AppComponent implements OnInit {
    public loading: boolean = true;
    private _isInitialized: boolean;
    private _messageSubscription: Subscription;
    constructor(
        private _appContext: AppContext,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _messageService: MessageService,
        public loadingService: LoaderService) {
        _router.events.subscribe((event: RouterEvent) => {
            this.navigationInterceptor(event);
        });

        this._appContext.isAuthenticated = appSettings.isAuthenticated;
        this._appContext.userDetails = new UserDetails();
        this._appContext.userDetails.userName = appSettings.userName;
        this._appContext.userDetails.firstName = appSettings.firstName;
        this._appContext.userDetails.lastName = appSettings.lastName;
        this._appContext.baseUrl = appSettings.baseUrl;

        if (this._appContext.isAuthenticated)
        {
            this._router.navigate(['mynetwork']);
        }
    }

    public ngOnInit() {

        
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        if (this._messageSubscription) {
            this._messageSubscription.unsubscribe();
        }
    }

    public navigationInterceptor(event: RouterEvent): void {
        if (event instanceof NavigationStart) {
            this.loadingService.loading = true;
        }
        if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
            if (event.url.indexOf('map') > -1) {
                setTimeout(() => {
                    this.loadingService.loading = false;
                }, 150); // give ability for map to redraw before show
            } else {
                this.loadingService.loading = false;
            }
            if (!this._isInitialized) {
                this._isInitialized = true;
                $("#page-loader").css({ top: '70px' });
            }
        }
        
    }
}
