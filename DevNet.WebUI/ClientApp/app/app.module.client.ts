import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { CoreModule } from './core';
import { SharedModule } from './shared';

import { HomeModule } from './home';
import { AuthenticationModule } from './authentication';
import { routing } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.module.shared';
import { ModalModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';

import './styles/site.scss';

@NgModule({
    bootstrap: [AppComponent],
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        ModalModule.forRoot(),
        CommonModule,
        CoreModule,
        FormsModule,
        HttpModule,
        SharedModule,
        HomeModule,
        AuthenticationModule,
        routing
    ]
})
export class AppModule {
}
