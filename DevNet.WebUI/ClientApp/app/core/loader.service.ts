﻿import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/share';

@Injectable()
export class LoaderService {
    public loading: boolean;
}