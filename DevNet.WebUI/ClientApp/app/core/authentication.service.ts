﻿import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
//import 'rxjs/add/operator/share';

import { CONSTANTS } from '../shared';
import { ApiService } from './index';  

@Injectable()
export class AuthenticationService {
    constructor(
        private _apiService: ApiService
    ) {

    }

    public login(model: any): Observable<any> {
        return this._apiService.requestPost(CONSTANTS.URI.login, model);
    }

    public forgot(model: any): Observable<any> {
        return this._apiService.requestPost(CONSTANTS.URI.forgot, model);
    }

    public register(model: any): Observable<any> {
        return this._apiService.requestPost(CONSTANTS.URI.register, model);
    }

    public logout(): Observable<any> {
        return this._apiService.requestPost(CONSTANTS.URI.logoff, "");
    }

    public reset(model: any): Observable<any> {
        return this._apiService.requestPost(CONSTANTS.URI.reset, model);
    }

}