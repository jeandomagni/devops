﻿import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/Rx';

import { AppContext, LoaderService, NotificationService } from './index';
import { CONSTANTS, GenericResponse } from '../shared';

@Injectable()
export class ApiService {
    public get apiUrl(): string {
        return `${this._appContext.baseUrl}/api/`;
    }
    public options: RequestOptionsArgs = {};
    constructor(
        private _http: Http,
        private _loaderService: LoaderService,
        private _appContext: AppContext,
        private _notificationService: NotificationService
    ) {
        this.setHeaders();
    }

    setHeaders() {
        var userdetails = this._appContext.userDetails;
        if (userdetails) {
            var bearerToken = 'Bearer ' + userdetails.token;
            this.options.headers = new Headers({
                'Authorization': bearerToken,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            });
        }
    }

    public post = (path: any, data:any) => {
        path = this.apiUrl + path;
        data = (data === '' || typeof (data) === 'undefined') ? '' : JSON.stringify(data);
        return this._http.post(path, data, this.options);
    }

    authCheck(path: any, data: any) {
        path = this.apiUrl + path;
        return this._http.get(path, this.options);
    }

    get(path: any) {                    
        path = this.apiUrl + path;
        return this._http.get(path, this.options);
    }

    getForLogin(path: any) {
        path = this.apiUrl + path;
        return this._http.get(path, this.options);
    }

    put(path: any, data: any) {
        path = this.apiUrl + path;
        data = (data === '' || typeof (data) === 'undefined') ? '' : JSON.stringify(data);
        return this._http.put(path, data, this.options);
    }

    delete(path: string, id?: string) {
        path = this.apiUrl + path;
        if (id === '' || typeof id === 'undefined' || id === null) {
            return this._http.delete(path, this.options);
        } else {
            return this._http.delete(`${path}/${id}`, this.options);
        }
    }

    //common behaviour for request
    private request(response: Observable<Response>): Observable<Subscription> {

        return response
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError = (errorResponse: any) => {
		var message = "";
		if(errorResponse.status == 401)
		{
			message = "Unauthorized user";
		} 
		else
		{
            message = JSON.parse(errorResponse._body).data;
		}
		this._notificationService.error(message);
        return Observable.throw(message);
    }

    public requestPost(url: string, data: any): Observable<Subscription> {
        var response = this.post(url, data);
        return this.request(response);
    }

    public requestGet(url: string): Observable<Subscription> {
        var response = this.get(url);
        return this.request(response);
    }

    public requestPut(url: string, data: any): Observable<Subscription> {
        var response = this.put(url, data);
        return this.request(response);
    }

    public requestDelete(url: string): Observable<Subscription> {
        var response = this.delete(url, '');
        return this.request(response);
    }
}