﻿import { Injectable, Inject, EventEmitter } from '@angular/core';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { LocalStorageService } from './index';
import { UserDetails } from '../shared';

@Injectable()
export class AppContext {
    private _userDetails: UserDetails = new UserDetails();
    get userDetails(): UserDetails {
        return this._userDetails;
    }
    set userDetails(theUserDetails: UserDetails) {
        this._userDetails = theUserDetails;
    }

    public isAuthenticated: boolean = false;
    public baseUrl: string;
}