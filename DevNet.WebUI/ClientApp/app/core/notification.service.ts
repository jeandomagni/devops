﻿import { Injectable } from '@angular/core';

import * as alertify from 'alertifyjs';

@Injectable()
export class NotificationService {
    error(message: string) {
        alertify.error(message);
    }
    success(message: string) {
        alertify.success(message);
    }
    info(message: string, callback?: Function) {
        alertify.confirm(message, callback);
    }
}