﻿import { Router } from "@angular/router";
import { ActivatedRoute, Params } from '@angular/router';
import { AppContext } from '../../core';

export class BaseRootComponent {
    constructor(protected _router: Router, protected _activatedRoute: ActivatedRoute, protected _appContext: AppContext)
    {

    }
}