﻿export class Message<T> {
    constructor(type: string, data?: T) {
        this.type = type;
        this.data = data;
    }

    public data?: T;
    public type: string;
}