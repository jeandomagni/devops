﻿export class UserDetails {
    public token: string;
    public userName: string;
    public firstName: string;
    public lastName: string;
}