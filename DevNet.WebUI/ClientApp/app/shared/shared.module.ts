﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';                            

import { NavMenuComponent } from './index';

@NgModule({
    imports: [CommonModule, RouterModule, FormsModule],
    declarations: [NavMenuComponent],
    exports: [CommonModule, FormsModule, NavMenuComponent],
    providers: []
})
export class SharedModule { }