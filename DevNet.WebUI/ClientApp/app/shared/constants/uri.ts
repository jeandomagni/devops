﻿export const URI = {                            

    login: 'account/login',

    logoff: 'account/logoff',

    forgot: 'account/forgot_password',

    reset: 'account/reset_password',
    
    register: 'account/register'
};