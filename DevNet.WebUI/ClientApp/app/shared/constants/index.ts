﻿import { URI } from './uri';
import { MessageTypes } from './message-types';

export const CONSTANTS = {
    URI,
    MessageTypes
};