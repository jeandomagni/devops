import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CoreModule } from './core';
import { SharedModule } from './shared';
                                                         
import { HomeModule } from './home';
import { AuthenticationModule } from './authentication';
import { routing } from './app.routing';

NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        CoreModule,
        FormsModule,
        HttpModule,
        SharedModule,
        HomeModule,
        AuthenticationModule,
        routing
    ]
})
export class AppModuleShared {
}
