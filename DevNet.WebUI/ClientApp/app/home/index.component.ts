﻿import { Component, OnInit, Output, EventEmitter, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { Router, RoutesRecognized, ActivatedRoute } from "@angular/router";
import { NotificationService, AuthenticationService, AppContext, MessageService } from '../core';

import { CONSTANTS, Message } from '../shared';
import * as $ from 'jquery';

@Component({
    selector: 'index',
    template: require('./index.component.html')
})
export class IndexComponent implements OnInit {
    private route: string = '';
   
    constructor(
        public appContext: AppContext,
        private _authenticationService: AuthenticationService,
        private _notificationService: NotificationService,
        private _routeService: Router,
        private _activatedRoute: ActivatedRoute,
        private _messageService: MessageService,
        private _changeDetectorRef: ChangeDetectorRef
    ) {

    }

    public ngOnInit() {
        //this.isAuthenticated = this.appContext.isAuthenticated;
        //this.name = this.appContext.userDetails.userName;
        this._routeService.events.subscribe((data: any) => {

        });

    }

    public isAuthenticated = () => {
        return this.appContext.isAuthenticated;
    }

    public name = () => {
        return this.appContext.userDetails.userName;
    }

    ngAfterViewInit() {

    }

    public logOff() {
        this._authenticationService.logout()
            .subscribe(response => {
                this._notificationService.success("You have successfully log off");
                window.location.reload();
                this._routeService.navigate(['']);
            }, error => {
                this._notificationService.error(error.message);
            });
    }

    navigate = (evt: MouseEvent, url: string) => {
    }
}
