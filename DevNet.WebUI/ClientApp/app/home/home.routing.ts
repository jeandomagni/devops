﻿import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IndexComponent, MyNetworkComponent } from './index';

export const routing: ModuleWithProviders = RouterModule.forChild([
    { path: '', component: IndexComponent },
    { path: 'mynetwork', component: MyNetworkComponent }
]);

