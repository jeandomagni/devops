﻿import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { AppContext, NotificationService, AuthenticationService } from '../../core';
import { SignupModel } from './signup.model';
import { BaseRootComponent } from './../../shared';

@Component({
    selector: 'signup',
    template: require('./signup.component.html')
})
export class SignUpComponent extends BaseRootComponent {
    public model: SignupModel = new SignupModel();
    public submitted: boolean = true;
    public privacyPolicy: boolean = false;
    public myForm: any;
    constructor(
        private _authenticationService: AuthenticationService,
        router: Router,
        appContext: AppContext,
        activatedRoute: ActivatedRoute,
        private _notificationService: NotificationService) {
        super(router, activatedRoute, appContext);
    }

    onSubmit() {
        this.submitted = true;
        this._authenticationService.register(this.model)
            .subscribe(response => {
                this._notificationService.success("Register success");
                this._appContext.isAuthenticated = true;
                this._appContext.userDetails.userName = this.model.email;
                this._appContext.userDetails.firstName = this.model.firstName;
                this._appContext.userDetails.lastName = this.model.lastName;
                this._router.navigate(['mynetwork']);
                this.submitted = false;
            }, error => {
                this.submitted = false;
            });
    }

    acceptPolicy() {
        this.privacyPolicy = !this.privacyPolicy;
        this.submitted = !this.privacyPolicy;
    }

    navigateToLogin = () => {
        //this._router.navigate(this._translateService.translateUrl(['login']))
    }
}
