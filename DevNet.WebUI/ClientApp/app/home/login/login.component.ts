﻿import { Component, Inject, } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { LoginModel } from './login.model';
import { AppContext, NotificationService, AuthenticationService } from '../../core';
import { BaseRootComponent } from './../../shared';

@Component({
    selector: 'login',
    template: require('./login.component.html'),
})
export class LoginComponent extends BaseRootComponent {
    public model: LoginModel = new LoginModel();
    public submitted: boolean;

    constructor(
        private _authenticationService: AuthenticationService,
        router: Router,
        appContext: AppContext,
        activatedRoute: ActivatedRoute,
        private _notificationService: NotificationService) {
        super(router, activatedRoute, appContext);
    }

    onSubmit() {
        this.submitted = true;
        this._authenticationService.login(this.model)
            .subscribe(response => {
                this._notificationService.success("Login success");
                this._appContext.isAuthenticated = true;
                this._appContext.userDetails.userName = this.model.userName;
                this._appContext.userDetails.firstName = response.data.firstName;
                this._appContext.userDetails.lastName = response.data.lastName;
                this._router.navigate(['mynetwork'])
                this.submitted = false;
            }, error => {
                this.submitted = false;
            });
    }

    public navigateToSignup = () => {
        //this._router.navigate(this._translateService.translateUrl(['signup']))
    }
    public navigateToForgot = () => {
        //this._router.navigate(this._translateService.translateUrl(['forgot']))
    }
}
