﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';   
               

import { SharedModule } from '../shared';
import { SignUpComponent } from './signup';
import { LoginComponent } from './login';
import {
    IndexComponent,
    MyNetworkComponent,
    routing
} from './index';

@NgModule({
    imports: [CommonModule, FormsModule, SharedModule, routing],
    declarations: [IndexComponent, SignUpComponent, LoginComponent, MyNetworkComponent],
    exports: [],
    providers: [],
    entryComponents: []
})
export class HomeModule { }