﻿import { Component, OnInit, Output, EventEmitter, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { Router, RoutesRecognized, ActivatedRoute } from "@angular/router";
import { NotificationService, AuthenticationService, AppContext, MessageService } from '../core';

import { CONSTANTS, Message } from '../shared';
import * as $ from 'jquery';

@Component({
    selector: 'mynetwork',
    template: require('./mynetwork.component.html')
})
export class MyNetworkComponent implements OnInit {
    private route: string = '';

    constructor(
        public appContext: AppContext,
        private _authenticationService: AuthenticationService,
        private _notificationService: NotificationService,
        private _routeService: Router,
        private _activatedRoute: ActivatedRoute,
        private _messageService: MessageService,
        private _changeDetectorRef: ChangeDetectorRef
    ) {

    }

    public ngOnInit() {
        //this.isAuthenticated = this.appContext.isAuthenticated;
        //this.name = this.appContext.userDetails.userName;
        this._routeService.events.subscribe((data: any) => {

        });

    }

    public isAuthenticated = () => {
        return this.appContext.isAuthenticated;
    }

    public name = () => {
        return this.appContext.userDetails.userName;
    }

    ngAfterViewInit() {

    }

    public logOff() {
        this._authenticationService.logout()
            .subscribe(response => {
                this._notificationService.success("You have successfully log off");
                window.location.reload();
                this._routeService.navigate([`login`]);
            }, error => {
                this._notificationService.error(error.message);
            });
    }

    navigate = (evt: MouseEvent, url: string) => {
    }

    public logout = () => {
        this._authenticationService.logout().subscribe(() => {
            this._routeService.navigate(['']);
        });
    }
}
