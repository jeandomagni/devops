/// <reference path="../googlemaps/google.maps.d.ts" />

declare namespace MapIcons {
    export class MarkerLabel extends google.maps.Marker {

    }

    export var MAP_PIN: string;
    export var SQUARE_PIN: string;
    export var SHIELD: string;
    export var ROUTE: string;
    export var SQUARE: string;
    export var SQUARE_ROUNDED: string;

    export function Marker(options: any): void;
    export function inherits(childCtor, parentCtor): void;
    export function initMapIcons(): void;
}

declare module 'map-icons' {
    export = MapIcons;
}