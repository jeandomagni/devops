﻿/// <reference path="../alertify/alertify.d.ts" />
declare module 'alertifyjs' {
    export = alertify;
}

declare module 'marker-clusterer-plus' {
    export = MarkerClusterer;
}

interface JQuery {
    niceSelect();
    niceSelect(param: string);
} 