﻿using DevNet.Core.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevNet.WebUI.Controllers
{
    public class BaseController : Controller
    {
        public readonly UserManager<User> _userManager;
        public string CurrentUserId { get; set; }
        public BaseController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        //Before action executed
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (User.Identity.IsAuthenticated)
            {
                CurrentUserId = _userManager.GetUserId(User);
                var user = GetUserById(CurrentUserId);
                ViewBag.FirstName = user.FirstName;
                ViewBag.LastName = user.LastName;

            }
            ViewBag.IpAddress = Request.HttpContext.Connection.RemoteIpAddress;
            ViewBag.BaseUrl = $"{Request.Scheme}://{Request.Host}/";
            
        }

        public User GetUserById(string userId)
        {
            return _userManager.Users.FirstOrDefault(x => x.Id == CurrentUserId); ;
        }

        public User GetUserByName(string userName)
        {
            return _userManager.Users.FirstOrDefault(x => x.UserName == userName); ;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
