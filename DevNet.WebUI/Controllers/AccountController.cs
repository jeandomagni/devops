﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using DevNet.Core.Models.Entities;
using DevNet.Core.Interfaces.Services;
using DevNet.Core.Models;
using Microsoft.AspNetCore.Http;
using DevNet.Core.Models.Account;
using DevNet.Core.Models.Emails;
using Hangfire;

namespace DevNet.WebUI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger _logger;
        private readonly IEmailService _emailService;

        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILoggerFactory loggerFactory,
            IEmailService emailService) : base(userManager)
        {
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _emailService = emailService;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation(1, "User logged in.");
                    var user = GetUserByName(model.UserName);
                    return Json(new GenericResponse<LoginResponse>(new LoginResponse { FirstName = user.FirstName, LastName = user.LastName }));
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning(2, "User account locked out.");
                    return BadRequest(new ErrorResponse<string>("User account locked out."));
                }
            }

            return BadRequest(new ErrorResponse<string>("Login failed"));
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    BackgroundJob.Enqueue(() => _emailService.SendRegisterEmail(new RegisterEmail("Account created", model.Email)
                    {
                        LoginUrl = $"{Request.Scheme}://{Request.Host}/login",
                        ResetUrl = $"{Request.Scheme}://{Request.Host}/forgot",
                        Login = model.Email,
                        Password = model.Password
                    }));

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                    // Send an email with this link
                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                    //    $"Please confirm your account by clicking this link: <a href='{callbackUrl}'>link</a>");
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User created a new account with password.");
                    return Json(new GenericResponse<bool>());
                }
                AddErrors(result);
                return BadRequest(new ErrorResponse<string>(string.Join("\n", result.Errors.Select(x => x.Description).ToList())));

            }
            var errorList = string.Join("\n", ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList());
            // If we got this far, something failed, redisplay form
            return BadRequest(new ErrorResponse<string>(errorList));
        }

        //
        // POST: /Account/LogOff
        [HttpGet("logout")]
        public async Task<IActionResult> Logout(string lang)
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            var l = lang == "en" ? "" : lang;
            return Redirect($"/{l}");
        }

        [HttpPost("logoff")]
        public async Task<GenericResponse<bool>> LogOff()
        {
            try
            {
                await _signInManager.SignOutAsync();
                _logger.LogInformation(4, "User logged out.");
                return new GenericResponse<bool>(true);
            }
            catch (Exception ex)
            {
                return new GenericResponse<bool>(false);
            }
        }

        // GET: /Account/ConfirmEmail
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost("forgot_password")]
        [AllowAnonymous]
        public async Task<GenericResponse<bool>> ForgotPassword([FromBody] ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null) //|| !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //return View("ForgotPasswordConfirmation");
                    throw new Exception("This user does not exist");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Home", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                await _emailService.SendForgotEmail(new ForgotEmail("Reset Password", model.Email)
                {
                    ResetUrl = callbackUrl,
                    Email = model.Email
                });
                return new GenericResponse<bool>(true);
                //return View("ForgotPasswordConfirmation");
            }
            throw new Exception("Please enter valid email");
            // If we got this far, something failed, redisplay form
            //return View(model);
        }

        [HttpPost("reset_password")]
        [AllowAnonymous]
        public async Task<GenericResponse<bool>> ResetPassword([FromBody] ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new Exception("Error occured during processing");
            }
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                throw new Exception("User does not exist");
                // Don't reveal that the user does not exist
                //return RedirectToAction(nameof(HomeController.ResetPasswordConfirmation), "Account");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return new GenericResponse<bool>(true);
            }
            AddErrors(result);
            throw new Exception("Error occured during processing");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<User> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
